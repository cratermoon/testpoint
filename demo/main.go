package main

import (
	"fmt"
	"math/rand"
	"time"
)

func greetStandard() Greeter {
	// Greet in the standard way
	g := StandardGreeter{}
	fmt.Printf("Standard: ")
	g.Greet()
	return g
}

func greetMock() Greeter {
	// Now Mock the greeter
	mg := MockGreeter{}
	fmt.Printf("Mock: ")
	mg.Greet()
	return mg
}

type eventRecord struct {
	msg string
	ts  time.Time
}

func greetTest(actual, mock Greeter, x int) {
	events := make([]eventRecord, 0)
	// combine the two into a testpoint
	tpg := NewTestPointGreeter(actual, mock)
	fmt.Printf("Test point, mocking=%t:", tpg.MockEnabled())
	tpg.Greet()
	tpg.OnEvent(func(e string) {
		events = append(events,
			eventRecord{
				msg: e,
				ts:  time.Now(),
			})
	})
	tpg.EnableMocking()
	fmt.Printf("Test point, mocking=%t: ", tpg.MockEnabled())
	for i := 0; i < x; i++ {
		time.Sleep(time.Duration(i) * time.Second / 10)
		tpg.Greet()
	}
	tpg.DisableMocking()
	fmt.Printf("Test point, mocking=%t: ", tpg.MockEnabled())
	tpg.Greet()

	for i, m := range events {
		fmt.Printf("Event %d: %s at %s\n", i, m.msg, m.ts.Format(time.StampMicro))
	}

}
func greetAllTheThings() {
	g := greetStandard()
	mg := greetMock()
	greetTest(g, mg, rand.Intn(7)+1)
}

func main() {
	rand.Seed(time.Now().UnixNano())
	greetAllTheThings()
}
