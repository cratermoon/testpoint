package main

import "fmt"

// StandardGreeter Greets with the standard greeting
type StandardGreeter struct {
}

// Greet implements the method from Greeter
func (g StandardGreeter) Greet() {
	fmt.Println("Hello, world!")
}
