package main

import (
	"fmt"
)

// MockGreeter is a Greeter that speaks mockingly
type MockGreeter struct {
}

// Greet implements the method on Greeter interface
func (g MockGreeter) Greet() {
	fmt.Println("hElLO, wOrLd!")
}

// TestPointGreeter is a TestPoint that forwards to other Greeter
type TestPointGreeter struct {
	actual    Greeter
	mock      Greeter
	current   Greeter
	enabled   bool
	Listeners []func(string)
	count     *int
}

// NewTestPointGreeter builds a TestPointGreeter with the given other Greeters
func NewTestPointGreeter(actual, mock Greeter) *TestPointGreeter {
	c := 0
	return &TestPointGreeter{
		actual:    actual,
		mock:      mock,
		current:   actual,
		Listeners: make([]func(string), 0),
		count:     &c,
	}
}

// Greet implements the function from Greeter
func (tpg TestPointGreeter) Greet() {
	if tpg.enabled {
		for _, f := range tpg.Listeners {
			f(fmt.Sprintf("greeted visitor %d", *tpg.count))
		}
	}
	tpg.current.Greet()
	*tpg.count++
}

// EnableMocking implements the function from TestPoint
func (tpg *TestPointGreeter) EnableMocking() error {
	if !tpg.enabled {
		tpg.current = tpg.mock
		tpg.enabled = true
	}
	return nil
}

// DisableMocking implements the function from TestPoint
func (tpg *TestPointGreeter) DisableMocking() error {
	if tpg.enabled {
		tpg.current = tpg.actual
		tpg.enabled = false
	}
	return nil
}

// MockEnabled implements the function from TestPoint
func (tpg TestPointGreeter) MockEnabled() bool {
	return tpg.enabled
}

// OnEvent implements the TestPoint OnEvent method
func (tpg *TestPointGreeter) OnEvent(consume func(string)) {
	tpg.Listeners = append(tpg.Listeners, consume)
}
