package main

// Greeter says hello!
type Greeter interface {
	Greet()
}
