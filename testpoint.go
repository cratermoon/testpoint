package testpoint

// TestPoint is the interface for enabling live testing
type TestPoint interface {
	EnableMocking() error
	DisableMocking() error
	MockEnabled() bool
	OnEvent(func(string))
}
